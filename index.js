const express = require("express");
const path = require("path");
const mongoose = require('mongoose');

const app = express();

const port = 8000;

const {
    dicing,
    gettingDH,
    gettingPH,
    gettingVH
} = require("./app/controllers/app.controller");

const userRouter = require("./app/routes/user.router");
const diceHistoryRouter = require("./app/routes/diceHistory.router");
const prizeRouter = require("./app/routes/prize.router");
const voucherRouter = require("./app/routes/voucher.router");
const prizeHistoryRouter = require("./app/routes/prizeHistory.router");
const voucherHistoryRouter = require("./app/routes/voucherHistory.router");

app.use(express.json());

app.use(express.static(__dirname + "/app/views"));

app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
});

app.use((req, res, next) => {
    console.log("Request method:", req.method);

    next();
});

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Dice")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });

app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/LuckyDice.html"))
})

app.get("/random-number", (req, res) => {
    var randomNumber = Math.floor(Math.random() * 6 + 1);
    res.status(200).json({
        number: randomNumber
    })
});

app.get("/devcamp-lucky-dice/dice-history", gettingDH);
app.get("/devcamp-lucky-dice/prize-history", gettingPH);
app.get("/devcamp-lucky-dice/voucher-history", gettingVH);

app.post("/devcamp-lucky-dice/dice", dicing);

app.use("/api/v1/users", userRouter);
app.use("/api/v1/dice-histories", diceHistoryRouter);
app.use("/api/v1/prizes", prizeRouter);
app.use("/api/v1/vouchers", voucherRouter);
app.use("/api/v1/prize-histories", prizeHistoryRouter);
app.use("/api/v1/voucher-histories", voucherHistoryRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
});