const phModel = require("../models/prizeHistory.model");
const mongoose = require("mongoose");

const createPH = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        user,
        prize
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        });
    }

    if (!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            message: "prize ID không hợp lệ"
        });
    }

    try {
        // B3: Xu ly du lieu
        var newPH = {
            user: user,
            prize: prize
        }

        const result = await phModel.create(newPH);

        return res.status(201).json({
            message: "Tạo prize history thành công",
            data: result
        });
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllPHs = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let user = req.query.user;
    // B2: Validate du lieu
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if (user) {
        condition.user = user;
    }

    try {
        const result = await phModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách prize history thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getPHById = async (req, res) => {
    // B1: Thu thap du lieu
    const phId = req.params.phId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(phId)) {
        return res.status(400).json({
            message: "prize history ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await phModel.findById(phId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin prize history thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin prize history"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updatePHById = async (req, res) => {
    // B1: Thu thap du lieu
    const phId = req.params.phId;

    const {
        user,
        prize
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(phId)) {
        return res.status(400).json({
            message: "prize history ID không hợp lệ"
        })
    }

    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    if (prize && !mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            message: "prize ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdatePH = {};
        if (user) {
            newUpdatePH.user = user;
        }
        if (prize) {
            newUpdatePH.prize = prize;
        }

        const result = await phModel.findByIdAndUpdate(phId, newUpdatePH);

        if (result) {
            const finalResult = await phModel.findById(phId);
            return res.status(200).json({
                message: "Update thông tin prize history thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin prize history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deletePHById = async (req, res) => {
    // B1: Thu thap du lieu
    const phId = req.params.phId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(phId)) {
        return res.status(400).json({
            message: "prize history ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await phModel.findByIdAndRemove(phId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin prize history thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin prize history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createPH,
    getAllPHs,
    getPHById,
    updatePHById,
    deletePHById
}
