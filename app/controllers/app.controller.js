const userModel = require("../models/user.model");
const dhModel = require('../models/dice.history.model');
const voucherModel = require("../models/voucher.model");
const vhModel = require("../models/voucherHistory.model");
const prizeModel = require("../models/prize.model");
const phModel = require("../models/prizeHistory.model");
const diceHistoryModel = require("../models/dice.history.model");
const mongoose = require("mongoose");

const dicing = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        username,
        firstname,
        lastname
    } = req.body;

    // B2: Validate du lieu
    if (!username) {
        return res.status(400).json({
            message: "Yêu cầu user name"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            message: "Yêu cầu firstname"
        })
    }

    if (!lastname) {
        return res.status(400).json({
            message: "Yêu cầu lastname"
        })
    }

    // B3: Xu ly du lieu
    try {
        //tạo dice history
        var resultCreateDH = null;

        const findUsernameResult = await userModel.findOne({ username: username });

        if (findUsernameResult) {
            var newDH1 = {
                user: findUsernameResult._id,
                dice: Math.floor(Math.random() * 6 + 1)
            }

            resultCreateDH = await dhModel.create(newDH1);
        } else {
            var newUser = {
                username: username,
                firstname: firstname,
                lastname: lastname
            }

            const resultCreateUser = await userModel.create(newUser);

            var newDH2 = {
                user: resultCreateUser._id,
                dice: Math.floor(Math.random() * 6 + 1)
            }

            resultCreateDH = await dhModel.create(newDH2);
        }

        //tạo voucher
        var voucher = null;

        if (resultCreateDH.dice > 3) {
            const count = await voucherModel.count();
            var random = Math.floor(Math.random() * count);
            voucher = await voucherModel.findOne().skip(random);

            var newVH = {
                user: resultCreateDH.user,
                voucher: voucher
            }
            const resultCreateVH = await vhModel.create(newVH);
        }

        //tạo prize
        var prize = null;
        const userDHs = await diceHistoryModel.find({ user: resultCreateDH.user });
        const last3 = userDHs.slice(-3);
        var check = last3.every(e => e.dice > 3);
        if (check) {
            const countP = await prizeModel.count();
            var randomP = Math.floor(Math.random() * countP);
            prize = await prizeModel.findOne().skip(randomP);

            var newPH = {
                user: resultCreateDH.user,
                prize: prize
            }

            const resultCreatePH = await phModel.create(newPH);
        }

        return res.status(200).json({
            voucher: voucher,
            dice: resultCreateDH.dice,
            prize: prize
        })

    } catch (error) {
        //console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const gettingDH = async (req, res) => {
    // B1: Thu thap du lieu
    const username = req.query.username;

    // B3: Xu ly du lieu
    try {
        const findUsernameResult = await userModel.findOne({ username: username });

        var dhArr = [];
        if (findUsernameResult) {
            dhArr = await dhModel.find({ user: findUsernameResult._id });
        }
        return res.status(200).json({
            diceHistory: dhArr
        })

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const gettingPH = async (req, res) => {
    // B1: Thu thap du lieu
    const username = req.query.username;

    // B3: Xu ly du lieu
    try {
        const findUsernameResult = await userModel.findOne({ username: username });

        var phArr = [];
        if (findUsernameResult) {
            phArr = await phModel.find({ user: findUsernameResult._id });
        }
        return res.status(200).json({
            prizeHistory: phArr
        })

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const gettingVH = async (req, res) => {
    // B1: Thu thap du lieu
    const username = req.query.username;

    // B3: Xu ly du lieu
    try {
        const findUsernameResult = await userModel.findOne({ username: username });

        var vhArr = [];
        if (findUsernameResult) {
            vhArr = await vhModel.find({ user: findUsernameResult._id });
        }
        return res.status(200).json({
            voucherHistory: vhArr
        })

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    dicing,
    gettingDH,
    gettingPH,
    gettingVH
};