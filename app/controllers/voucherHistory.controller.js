const vhModel = require("../models/voucherHistory.model");
const mongoose = require("mongoose");

const createVH = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        user,
        voucher
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        });
    }

    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            message: "voucher ID không hợp lệ"
        });
    }

    try {
        // B3: Xu ly du lieu
        var newVH = {
            user: user,
            voucher: voucher
        }

        const result = await vhModel.create(newVH);

        return res.status(201).json({
            message: "Tạo voucher history thành công",
            data: result
        });
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllVHs = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let user = req.query.user;
    // B2: Validate du lieu
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if (user) {
        condition.user = user;
    }
    
    try {
        const result = await vhModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách voucher history thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getVHById = async (req, res) => {
    // B1: Thu thap du lieu
    const vhId = req.params.vhId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(vhId)) {
        return res.status(400).json({
            message: "voucher history ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await vhModel.findById(vhId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin voucher history thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin voucher history"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateVHById = async (req, res) => {
    // B1: Thu thap du lieu
    const vhId = req.params.vhId;

    const {
        user,
        voucher
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(vhId)) {
        return res.status(400).json({
            message: "voucher history ID không hợp lệ"
        })
    }

    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    if (voucher && !mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            message: "voucher ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateVH = {};
        if (user) {
            newUpdateVH.user = user;
        }
        if (voucher) {
            newUpdateVH.voucher = voucher;
        }

        const result = await vhModel.findByIdAndUpdate(vhId, newUpdateVH);

        if (result) {
            const finalResult = await vhModel.findById(vhId);
            return res.status(200).json({
                message: "Update thông tin voucher history thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin voucher history"
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteVHById = async (req, res) => {
    // B1: Thu thap du lieu
    const vhId = req.params.vhId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(vhId)) {
        return res.status(400).json({
            message: "voucher history ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await vhModel.findByIdAndRemove(vhId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin voucher history thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin voucher history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createVH,
    getAllVHs,
    getVHById,
    updateVHById,
    deleteVHById
}
