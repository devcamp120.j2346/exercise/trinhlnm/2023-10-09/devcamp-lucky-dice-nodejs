const prizeModel = require("../models/prize.model");
const mongoose = require("mongoose");

const createPrize = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        name,
        description
    } = req.body;

    // B2: Validate du lieu
    if (!name) {
        return res.status(400).json({
            message: "Yêu cầu name"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newPrize = {
            name: name,
            description: description
        }

        const result = await prizeModel.create(newPrize);

        return res.status(201).json({
            message: "Tạo prize thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllPrizes = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await prizeModel.find();

        return res.status(200).json({
            message: "Lấy danh sách prize thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getPrizeById = async (req, res) => {
    // B1: Thu thap du lieu
    const prizeId = req.params.prizeId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: "Prize ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await prizeModel.findById(prizeId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin prize thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin prize"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updatePrizeById = async (req, res) => {
    // B1: Thu thap du lieu
    const prizeId = req.params.prizeId;

    const {
        name,
        description
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: "Prize ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdatePrize = {};
        if (name) {
            newUpdatePrize.name = name;
        }
        if (description) {
            newUpdatePrize.description = description;
        }

        const result = await prizeModel.findByIdAndUpdate(prizeId, newUpdatePrize);

        if (result) {
            const finalResult = await prizeModel.findById(prizeId);
            return res.status(200).json({
                message: "Update thông tin prize thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin prize"
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deletePrizeById = async (req, res) => {
    // B1: Thu thap du lieu
    const prizeId = req.params.prizeId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: "Prize ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await prizeModel.findByIdAndRemove(prizeId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin prize thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin prize"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createPrize,
    getAllPrizes,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}
