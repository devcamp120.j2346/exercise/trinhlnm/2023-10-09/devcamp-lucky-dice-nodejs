const voucherModel = require("../models/voucher.model");
const mongoose = require("mongoose");

const createVoucher = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        code,
        discount,
        note
    } = req.body;

    // B2: Validate du lieu
    if (!discount) {
        return res.status(400).json({
            message: "Yêu cầu discount"
        })
    }
    if (isNaN(discount) || discount <= 0) {
        return res.status(400).json({
            message: "Discount không hợp lệ"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newVoucher = {
            code: Math.random().toString(16).slice(2),
            discount: discount,
            note: note
        }

        const result = await voucherModel.create(newVoucher);

        return res.status(201).json({
            message: "Tạo voucher thành công",
            data: result
        });
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllVouchers = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.find();

        return res.status(200).json({
            message: "Lấy danh sách voucher thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherId = req.params.voucherId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "voucher ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.findById(voucherId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin voucher thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin voucher"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherId = req.params.voucherId;

    const {
        code,
        discount,
        note
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "voucher ID không hợp lệ"
        })
    }

    if (discount && (isNaN(discount) || discount <= 0)) {
        return res.status(400).json({
            message: "Discount không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateVoucher = {};
        if (code) {
            newUpdateVoucher.code = code;
        }
        if (discount) {
            newUpdateVoucher.discount = discount;
        }
        if (note) {
            newUpdateVoucher.note = note;
        }

        const result = await voucherModel.findByIdAndUpdate(voucherId, newUpdateVoucher);

        if (result) {
            const finalResult = await voucherModel.findById(voucherId);
            return res.status(200).json({
                message: "Update thông tin voucher thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherId = req.params.voucherId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "voucher ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.findByIdAndRemove(voucherId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin voucher thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}
