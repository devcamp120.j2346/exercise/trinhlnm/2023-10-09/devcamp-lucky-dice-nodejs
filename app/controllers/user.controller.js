const userModel = require("../models/user.model");
const mongoose = require("mongoose");

const createUser = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        username,
        firstname,
        lastname
    } = req.body;

    // B2: Validate du lieu
    if (!username) {
        return res.status(400).json({
            message: "Yêu cầu user name"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            message: "Yêu cầu firstname"
        })
    }

    if (!lastname) {
        return res.status(400).json({
            message: "Yêu cầu lastname"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newUser = {
            username: username,
            firstname: firstname,
            lastname: lastname
        }

        const result = await userModel.create(newUser);

        return res.status(201).json({
            message: "Tạo user thành công",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error

        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllUsers = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await userModel.find();

        return res.status(200).json({
            message: "Lấy danh sách user thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findById(userId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin user thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    const {
        username,
        firstname,
        lastname
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateUser = {};
        if (username) {
            newUpdateUser.username = username;
        }
        if (firstname) {
            newUpdateUser.firstname = firstname;
        }
        if (lastname) {
            newUpdateUser.lastname = lastname;
        }

        const result = await userModel.findByIdAndUpdate(userId, newUpdateUser);

        if (result) {
            const finalResult = await userModel.findById(userId);
            return res.status(200).json({
                message: "Update thông tin user thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findByIdAndRemove(userId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin user thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
}
