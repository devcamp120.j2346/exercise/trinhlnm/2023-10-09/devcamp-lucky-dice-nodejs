//import thư viện mongoose
const mongoose = require('mongoose');

// import DH model
const dhModel = require('../models/dice.history.model');

// const create DH 
const createDH = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        user,
        dice
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    var newDH = {
        user: user,
        dice: Math.floor(Math.random() * 6 + 1)
    }

    try {
        const result = await dhModel.create(newDH);

        return res.status(201).json({
            message: "Tạo dice history thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllDHs = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let user = req.query.user;
    // B2: Validate du lieu
    if (user && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if (user) {
        condition.user = user;
    }

    try {
        const result = await dhModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách dice history thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getDHById = async (req, res) => {
    //B1: thu thập dữ liệu
    var dhId = req.params.dhId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(dhId)) {
        return res.status(400).json({
            message: "Dice history ID không hợp lệ"
        })
    }
    // B3: Xử lý dữ liệu
    try {
        const result = await dhModel.findById(dhId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin dice history thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin dice history"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateDH = async (req, res) => {
    //B1: thu thập dữ liệu
    var dhId = req.params.dhId;

    const {
        user,
        dice
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(dhId)) {
        return res.status(400).json({
            message: "Dice history ID không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    if (dice && (dice < 1 || dice > 6)) {
        return res.status(400).json({
            message: "Dice không hợp lệ"
        })
    }

    //B3: thực thi model
    try {
        var newUpdateDH = {};
        if (user) {
            newUpdateDH.user = user;
        }
        if (dice) {
            newUpdateDH.dice = dice;
        }

        const result = await dhModel.findByIdAndUpdate(dhId, newUpdateDH);

        if (result) {
            const finalResult = await dhModel.findById(dhId);
            return res.status(200).json({
                message: "Update thông tin dice history thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin dice history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}


const deleteDH = async (req, res) => {
    //B1: Thu thập dữ liệu
    var dhId = req.params.dhId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(dhId)) {
        return res.status(400).json({
            message: "Dice history ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await dhModel.findByIdAndRemove(dhId);

        if (result) {
            return res.status(200).json({
                message: "Xóa dice history thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy dice history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createDH,
    getAllDHs,
    getDHById,
    updateDH,
    deleteDH
}