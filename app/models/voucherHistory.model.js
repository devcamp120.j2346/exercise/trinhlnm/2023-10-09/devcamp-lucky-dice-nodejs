const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const voucherHistorySchema = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User"
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Voucher History", voucherHistorySchema);