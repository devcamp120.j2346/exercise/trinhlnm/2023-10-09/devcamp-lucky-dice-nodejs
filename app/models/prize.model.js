const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const prizeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false,
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Prize", prizeSchema);