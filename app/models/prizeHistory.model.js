const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const prizeHistorySchema = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User"
    },
    prize: {
        type: mongoose.Types.ObjectId,
        ref: "Prize"
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Prize History", prizeHistorySchema);