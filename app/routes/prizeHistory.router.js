const express = require("express");

const router = express.Router();

const {
    createPH,
    getAllPHs,
    getPHById,
    updatePHById,
    deletePHById
} = require("../controllers/prizeHistory.controller");

router.get("/", getAllPHs);

router.post("/", createPH);

router.get("/:phId", getPHById);

router.put("/:phId", updatePHById);

router.delete("/:phId", deletePHById);

module.exports = router;

