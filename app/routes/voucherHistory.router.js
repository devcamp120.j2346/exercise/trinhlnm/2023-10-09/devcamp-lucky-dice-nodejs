const express = require("express");

const router = express.Router();

const {
    createVH,
    getAllVHs,
    getVHById,
    updateVHById,
    deleteVHById
} = require("../controllers/voucherHistory.controller");

router.get("/", getAllVHs);

router.post("/", createVH);

router.get("/:vhId", getVHById);

router.put("/:vhId", updateVHById);

router.delete("/:vhId", deleteVHById);

module.exports = router;

