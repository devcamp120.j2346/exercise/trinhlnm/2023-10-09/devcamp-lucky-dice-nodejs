const express = require("express");

const router = express.Router();

const {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
} = require("../controllers/voucher.controller");

router.get("/", getAllVouchers);

router.post("/", createVoucher);

router.get("/:voucherId", getVoucherById);

router.put("/:voucherId", updateVoucherById);

router.delete("/:voucherId", deleteVoucherById);

module.exports = router;

