const express = require("express");

const router = express.Router();

const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
} = require("../controllers/user.controller");

router.get("/", getAllUsers);

router.post("/", createUser);

router.get("/:userId", getUserById);

router.put("/:userId", updateUserById);

router.delete("/:userId", deleteUserById);

module.exports = router;

