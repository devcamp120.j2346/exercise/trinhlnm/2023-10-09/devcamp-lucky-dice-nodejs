const express = require("express");

const router = express.Router();

const {
    createPrize,
    getAllPrizes,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
} = require("../controllers/prize.controller");

router.get("/", getAllPrizes);

router.post("/", createPrize);

router.get("/:prizeId", getPrizeById);

router.put("/:prizeId", updatePrizeById);

router.delete("/:prizeId", deletePrizeById);

module.exports = router;

