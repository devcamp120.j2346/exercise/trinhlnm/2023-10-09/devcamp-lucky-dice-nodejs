const express = require("express");

const router = express.Router();

const {
    createDH,
    getAllDHs,
    getDHById,
    updateDH,
    deleteDH
} = require("../controllers/diceHistory.controller");

router.get("/", getAllDHs);

router.post("/", createDH);

router.get("/:dhId", getDHById);

router.put("/:dhId", updateDH);

router.delete("/:dhId", deleteDH);

module.exports = router;

